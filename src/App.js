import React from 'react';
import './App.css';


class App extends React.Component{

  state = {
    data: '' 
  }

  handleChange = e =>{
    this.setState({data: e.target.value}) 
    console.log(this.state.data)
  }

  
  render(){

    const nodata = 'no data provided';


    return <div> 
            <input onChange={this.handleChange}/>
            <h1>{this.state.data||nodata}</h1>
           </div>
  }
}
export default App